# lego-crawler project

## Purpose
I did this project for my brother that is a huge fan of Lego and owns hundreds of sets. 
He is interested in watching over the price of the sets he wants to sell on bricklink. 
This simple program take input csv file with lego set numbers, downloads data from bricklink and exports it to excel.

## Technology

### Framework
I chose quarkus command line application to give it a try as so far I was mostly working with Spring ecosystem. 
I wanted to go with graalvm and native image build. As my brother is running windows I had to build the native image on windows.
I added kotlin as well to write the data classes without the java boilerplate clutterg. As graalvm does not have Java 14 support yet and I am not sure about lombok and quarkus compatibility, 
this was my next choice.

### Web parsing
For html parsing I used Jsoup. I could parse the javascript with the regex as what I need to do was nothing fancy but I wanted to try the javascript processor.
I went for graal with truffle. The javascript language needs to be added to the native image - as an argument during the build.
The only downside is that this it does not work when running `quarkus:dev` for some reason.

### Excel Export
My first choice for excel export was of course Apache POI. I faced issue later during the runtime when saving the file but only with the native image.
Similar to this one https://github.com/quarkusio/quarkus/issues/10891. Obviosly quarkus does not have support for Apache POI (althoug it has support for apache tika which uses apacha poi).
My second choice was alibaba's easyexcel. With the documentation full in chinese it was a bit challenge. But the API looked good.
I definitely prefered it over Apache POI, with the concept of annotations and converters it was quite promising. 
Unfortunately I faced another issue during the compile time - https://github.com/oracle/graal/issues/2177. Looks like there is no solution.
I also tried flatpack excel with no luck. Finally I went with fast excel which seems to be working.

### Pipeline
As I had to build the native image for windows I had to build it on a windows machine. Fortunately gitlab offers windows VMs as well.
I had to write some powershell scripts and for something I could have done in few minutes in bash I spend several hours.
It's not event compatible with cmd where the visual studio is running, everything is so unintuitive, the error messages were useless. 
I hope I won't have to do that ever again. 

## Notes

### Building native Image on Windows:
1. Install Visual Studio with C (MSVC is necessary)
2. Run `x64 Native Tools Command Prompt for VS 2019` 
3. Run `mvn -DskipTests package -Pnative` from that command line
4. If compilation fails because of Kotlin, do `mvn clean` first
