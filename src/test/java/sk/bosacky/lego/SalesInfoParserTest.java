package sk.bosacky.lego;

import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import sk.bosacky.lego.model.SalesInfo;
import sk.bosacky.lego.model.SalesInfoDetail;

import javax.inject.Inject;

@QuarkusTest
public class SalesInfoParserTest {

    @Inject
    SalesInfoParser salesInfoParser;

    @Test
    public void priceGuideTest() throws Exception {
        var doc = TestUtils.getResourceAsDocument("price-guide.html");

        var salesInfo = salesInfoParser.parse(doc);

        var expected = new SalesInfo(
                new SalesInfoDetail(32, 36, 5.00, 13.41, 12.96, 19.99),
                new SalesInfoDetail(18, 18, 4.50, 8.15, 8.15, 11.00),
                new SalesInfoDetail(62, 113, 4.50, 17.74, 17.35, 55.95),
                new SalesInfoDetail(18, 18, 6.40, 9.93, 9.93, 16.21)
        );

        Assertions.assertEquals(expected, salesInfo);
    }

    @Test
    public void priceGuideWithMissingTest() throws Exception {
        var doc = TestUtils.getResourceAsDocument("price-guide-missing.html");

        var salesInfo = salesInfoParser.parse(doc);

        var expected = new SalesInfo(
                new SalesInfoDetail(10, 14, 14.25, 17.59, 17.28, 25.00),
                new SalesInfoDetail(0, 0, -1.0, -1.0, -1.0, -1.0),
                new SalesInfoDetail(79, 208, 10.79, 27.41, 26.50, 63.31),
                new SalesInfoDetail(2, 2, 15.00, 15.98, 15.98, 16.95)
        );

        Assertions.assertEquals(expected, salesInfo);
    }
}