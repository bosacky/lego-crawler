package sk.bosacky.lego;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.File;

public class TestUtils {
    private TestUtils() {
    }

    public static Document getResourceAsDocument(String resourceName) throws Exception {
        var resource = ClassLoader.getSystemClassLoader().getResource(resourceName);
        var input = new File(resource.toURI());
        return Jsoup.parse(input, "UTF-8", "http://example.com/");
    }
}
