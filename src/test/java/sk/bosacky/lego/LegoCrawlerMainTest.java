package sk.bosacky.lego;

import io.quarkus.test.junit.QuarkusTest;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;

@QuarkusTest
public class LegoCrawlerMainTest {

    @Inject
    LegoCrawlerMain main;

    @Test
    @Disabled
    public void testLegoCrawlerRunner() throws Exception {
        main.run("/home/pavel/Source/lego-crawler/src/main/resources/lego-set-minimal.csv");
    }
}
