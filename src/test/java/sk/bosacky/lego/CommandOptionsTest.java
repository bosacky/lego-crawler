package sk.bosacky.lego;

import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

@QuarkusTest
public class CommandOptionsTest {

    @Test
    public void testValidOptions() {
        var commandOptions = new CommandOptions("-i", "test.csv", "--output", "test.xslx");
        Assertions.assertEquals(2, commandOptions.size());
        Assertions.assertEquals("test.csv", commandOptions.getOption(CommandOption.INPUT).orElseThrow());
        Assertions.assertEquals("test.xslx", commandOptions.getOption(CommandOption.OUTPUT).orElseThrow());
    }

    @Test
    public void testOptionsWithAdditionalValue() {
        var commandOptions = new CommandOptions("-i", "test.csv", "thisshouldbedropped", "--output", "test.xslx");
        Assertions.assertEquals(2, commandOptions.size());
        Assertions.assertEquals("test.csv", commandOptions.getOption(CommandOption.INPUT).orElseThrow());
        Assertions.assertEquals("test.xslx", commandOptions.getOption(CommandOption.OUTPUT).orElseThrow());
    }


    @Test
    public void testMissingValue() {
        var commandOptions = new CommandOptions("-i", "--output", "test.xslx");
        Assertions.assertEquals(2, commandOptions.size());
        Assertions.assertEquals("", commandOptions.getOption(CommandOption.INPUT).orElseThrow());
        Assertions.assertEquals("test.xslx", commandOptions.getOption(CommandOption.OUTPUT).orElseThrow());
    }

    @Test
    public void testMissingCommand() {
        var commandOptions = new CommandOptions("DUMMYVALUECRAP", "--help");
        Assertions.assertEquals(1, commandOptions.size());
        Assertions.assertEquals("", commandOptions.getOption(CommandOption.HELP).orElseThrow());
    }
}
