package sk.bosacky.lego;

import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import sk.bosacky.lego.model.LegoSetInfo;
import sk.bosacky.lego.model.SalesInfo;
import sk.bosacky.lego.model.SalesInfoDetail;

import javax.inject.Inject;
import javax.inject.Named;
import java.io.File;
import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.List;

@QuarkusTest
public class ExcelExporterTest {

    private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd_HHmmss").withZone(ZoneId.systemDefault());

    @Inject
    ExcelExporter exporter;

    @Test
    public void testExporterCreatesFile() throws Exception {

        var legoSetInfo = new LegoSetInfo("12345", "Divoky pes", 225, 3, 1500,
                new SalesInfo(
                        new SalesInfoDetail(1, 1, 1.0, 1.0, 1.0, 1.0),
                        new SalesInfoDetail(1, 1, 1.0, 1.0, 1.0, 1.0),
                        new SalesInfoDetail(1, 1, 1.0, 1.0, 1.0, 1.0),
                        new SalesInfoDetail(1, 1, 1.0, 1.0, 1.0, 1.0)
                ),
                "www.zadecky.cz"
        );
        var dateTimeString = DATE_TIME_FORMATTER.format(Instant.now());
        var fileName = "lego-" + dateTimeString + ".xlsx";
        exporter.exportToExcel(List.of(legoSetInfo), fileName);

        var f = new File(fileName);
        Assertions.assertTrue(f.delete());
    }

}
