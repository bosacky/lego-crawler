package sk.bosacky.lego;

import io.quarkus.test.junit.QuarkusTest;
import org.graalvm.collections.Pair;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;

import static org.junit.jupiter.api.Assertions.*;

@QuarkusTest
public class BrickLinkParserTest {

    @Inject
    BrickLinkParser brickLinkParser;

    @Test
    public void countTupleParseMinifigsTest() {
        var tuple = brickLinkParser.parseCountTuple("25 Minifigs");
        assertEquals(Pair.create("Minifigs", 25), tuple);
    }

    @Test
    public void countTupleParseIncorrectTest() {
        var tuple = brickLinkParser.parseCountTuple("this is some bullshit");
        assertEquals(Pair.empty(), tuple);
    }

    @Test
    public void systemMaintenanceTest() throws Exception {
        var doc = TestUtils.getResourceAsDocument("system-maintenance.html");

        var exception = assertThrows(RuntimeException.class, () -> {
            brickLinkParser.parse("12345", doc);
        });

        var expectedMessage = "Lego title not found";
        var actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    public void noMinifigsTest() throws Exception {
        var doc = TestUtils.getResourceAsDocument("hovercraft.html");
        var setInfo = brickLinkParser.parse("42002", doc);
        assertEquals(0, setInfo.getMinifigsCount());
    }

    @Test
    public void noWeightTest() throws Exception {
        var doc = TestUtils.getResourceAsDocument("fishermap.html");
        var setInfo = brickLinkParser.parse("40066", doc);
        assertEquals(0, setInfo.getWeight());
    }
}
