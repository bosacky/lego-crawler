package sk.bosacky.lego;

import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import sk.bosacky.lego.model.Item;

import javax.inject.Inject;
import java.util.Optional;

@QuarkusTest
public class ItemParserTest {

    @Inject
    ItemParser itemParser;

    @Test
    public void extractItemDocTest() throws Exception {
        var doc = TestUtils.getResourceAsDocument("start-destroyer.html");

        var item = doc.select("script").stream()
                .filter(el -> el.childNodeSize() > 0)
                .map(el -> el.childNodes().get(0).toString())
                .map(scr -> itemParser.parseJs(scr))
                .filter(Optional::isPresent)
                .map(Optional::get)
                .findFirst().orElseThrow();

        var expected = new Item(
                120596,
                "S",
                "Set",
                "75033-1",
                "75033",
                "A",
                "A",
                "1",
                0,
                "J",
                "258",
                0,
                "//img.bricklink.com/ItemImage/ST/0/75033-1.t1.png",
                "//img.bricklink.com/ItemImage/SN/0/75033-1.png",
                "//img.bricklink.com/ItemImage/SL/75033-1.png",
                "//img.bricklink.com/ItemImage/SL/75033-1.png",
                "//img.bricklink.com/ItemImage/IT/0/75033-1.t1.png",
                "//img.bricklink.com/ItemImage/IN/0/75033-1.png",
                "//img.bricklink.com/ItemImage/OT/0/75033-1.t1.png",
                "//img.bricklink.com/ItemImage/ON/0/75033-1.png",
                "Star Destroyer");

        Assertions.assertEquals(expected, item);
    }


    @Test
    public void extractItemScriptTest() throws Exception {
        var stream = ClassLoader.getSystemClassLoader().getResourceAsStream("src.js");
        var item = itemParser.parseJs(new String(stream.readAllBytes()));

        var expected = new Item(
                120596,
                "S",
                "Set",
                "75033-1",
                "75033",
                "A",
                "A",
                "1",
                0,
                "J",
                "258",
                0,
                "//img.bricklink.com/ItemImage/ST/0/75033-1.t1.png",
                "//img.bricklink.com/ItemImage/SN/0/75033-1.png",
                "//img.bricklink.com/ItemImage/SL/75033-1.png",
                "//img.bricklink.com/ItemImage/SL/75033-1.png",
                "//img.bricklink.com/ItemImage/IT/0/75033-1.t1.png",
                "//img.bricklink.com/ItemImage/IN/0/75033-1.png",
                "//img.bricklink.com/ItemImage/OT/0/75033-1.t1.png",
                "//img.bricklink.com/ItemImage/ON/0/75033-1.png",
                "Star Destroyer");

        Assertions.assertEquals(expected, item.orElseThrow());
    }
}
