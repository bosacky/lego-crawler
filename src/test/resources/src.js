var _assoc_host = "http://www.bricklink.com";

var _var_item = {
    idItem: 120596
    , type: 'S'
    , typeName: 'Set'
    , itemno: '75033-1'
    , itemnoBase: '75033'
    , itemStatus: 'A'
    , invStatus: 'A'
    , itemSeq: '1'
    , idColorDefault: 0
    , typeImgDefault: 'J'
    , catID: '258'
    , idColorForPG: 0
    , strMainSImgUrl: '//img.bricklink.com/ItemImage/ST/0/75033-1.t1.png'
    , strMainLImgUrl: '//img.bricklink.com/ItemImage/SN/0/75033-1.png'
    , strLegacyLargeImgUrl: '//img.bricklink.com/ItemImage/SL/75033-1.png'
    , strLegacyLargeThumbImgUrl: '//img.bricklink.com/ItemImage/SL/75033-1.png'
    , strAssoc1ImgSUrl: '//img.bricklink.com/ItemImage/IT/0/75033-1.t1.png'
    , strAssoc1ImgLUrl: '//img.bricklink.com/ItemImage/IN/0/75033-1.png'
    , strAssoc2ImgSUrl: '//img.bricklink.com/ItemImage/OT/0/75033-1.t1.png'
    , strAssoc2ImgLUrl: '//img.bricklink.com/ItemImage/ON/0/75033-1.png'
    , strItemName: 'Star Destroyer'
};
var _var_iscolor_enabled = "N";
var _var_view_in_ounce = "N";
var _var_type_size = "C";
var _var_has_inventory = "Y";
var _var_isadmin = "N";
var _var_color_selected = -1;
var _var_ispg_available = 0;
var _login_id = "";
var _showEmailMarketingModal = false;
var _user_email = "";
_var_ispg_available = 1;

var _var_img_startidx = 0;
var _var_images = [];

var _var_search_option =
    {
        cCond: 'A'
        , strCountryShipsTo: ''
        , excludeStopListed: ('N' == 'Y')
        , excludeLeastFav: ('N' == 'Y')
        , showFlag: ('N' == 'Y')
        , showMoreOption: ('N' == 'Y')
        , sorttype: 1
        , rpp: 25
    };

var _var_pg_option =
    {
        sorttype: 1
        , group_by_currency: 'N' == 'Y' ? 1 : 0
        , exclude_incomplete: 'N' == 'Y' ? 1 : 0
        , precision: 2
        , country_filter: ''
        , region_filter: 0
        , show_flag: 'N' == 'Y' ? 1 : 0
        , show_bulk: 'N' == 'Y' ? 1 : 0
        , display_currency: 2
    };

var _var_inv_option =
    {
        sorttype: 1
        , show_invid: 'N' == 'Y' ? 1 : 0
        , show_matchcolor: 'Y' == 'Y' ? 1 : 0
        , show_pglink: 'N' == 'Y' ? 1 : 0
        , show_pcc: 'Y' == 'Y' ? 1 : 0
        , show_missingpcc: 'Y' == 'Y' ? 1 : 0
        , break_set: 0
        , break_minifigs: 0
    };

// Main Color Image
_var_images.push({
    isBig: true,
    url: '//img.bricklink.com/ItemImage/SN/0/75033-1.png',
    thumb_url: '//img.bricklink.com/ItemImage/ST/0/75033-1.t1.png',
    idColor: 0,
    typeItem: _var_item.type
});

if ((_var_item.type == "P" || _var_item.type == "G") && _var_item.strLegacyLargeImgUrl != "")
    _var_images.push({
        isBig: true,
        url: '//img.bricklink.com/ItemImage/SL/75033-1.png',
        thumb_url: '//img.bricklink.com/ItemImage/SL/75033-1.png',
        idColor: -1,
        typeItem: _var_item.type
    });
if (_var_item.strAssoc1ImgSUrl != "")
    _var_images.push({
        isBig: true,
        url: '//img.bricklink.com/ItemImage/IN/0/75033-1.png',
        thumb_url: '//img.bricklink.com/ItemImage/IN/0/75033-1.png',
        idColor: -1,
        typeItem: 'I'
    });
if (_var_item.strAssoc2ImgSUrl != "")
    _var_images.push({
        isBig: true,
        url: '//img.bricklink.com/ItemImage/ON/0/75033-1.png',
        thumb_url: '//img.bricklink.com/ItemImage/ON/0/75033-1.png',
        idColor: -1,
        typeItem: 'O'
    });


if (_var_iscolor_enabled == "Y")
    _var_images.push({
        isBig: false,
        url: 'SMALL_IMAGE',
        thumb_url: null,
        idColor: -1,
        typeItem: _var_item.type
    });

if (_var_item.type == "P" && brickList[_var_item.itemno])
    _var_images.push({isBig: false, url: '3D_IMAGE', idColor: -1, typeItem: _var_item.type});

//if ( _var_item.type != "P" && _var_item.type != "G" && _var_item.strLegacyLargeImgUrl != "" )
//	_var_images.push( { isBig: false, url: 'LARGE_IMAGE', thumb_url: null, idColor: -1 } );