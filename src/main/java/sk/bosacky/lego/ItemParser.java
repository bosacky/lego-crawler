package sk.bosacky.lego;

import org.graalvm.polyglot.Context;
import org.graalvm.polyglot.PolyglotException;
import org.graalvm.polyglot.Source;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sk.bosacky.lego.model.Item;

import javax.enterprise.context.Dependent;
import java.io.IOException;
import java.util.Optional;

@Dependent
public class ItemParser {
    private static final Logger LOG = LoggerFactory.getLogger(ItemParser.class);

    Optional<Item> parseJs(String source) {
        try (var context = Context.create()) {
            context.eval(Source.newBuilder("js", fixOpentipNotDefined(source), "src.js").build());
            var item = context.getBindings("js").getMember("_var_item");
            if (item == null) return Optional.empty();
            else
                return Optional.of(new Item(
                        Integer.parseInt(item.getMember("idItem").toString()),
                        item.getMember("type").toString(),
                        item.getMember("typeName").toString(),
                        item.getMember("itemno").toString(),
                        item.getMember("itemnoBase").toString(),
                        item.getMember("itemStatus").toString(),
                        item.getMember("invStatus").toString(),
                        item.getMember("itemSeq").toString(),
                        Integer.parseInt(item.getMember("idColorDefault").toString()),
                        item.getMember("typeImgDefault").toString(),
                        item.getMember("catID").toString(),
                        Integer.parseInt(item.getMember("idColorForPG").toString()),
                        item.getMember("strMainSImgUrl").toString(),
                        item.getMember("strMainLImgUrl").toString(),
                        item.getMember("strLegacyLargeImgUrl").toString(),
                        item.getMember("strLegacyLargeThumbImgUrl").toString(),
                        item.getMember("strAssoc1ImgSUrl").toString(),
                        item.getMember("strAssoc1ImgLUrl").toString(),
                        item.getMember("strAssoc2ImgSUrl").toString(),
                        item.getMember("strAssoc2ImgLUrl").toString(),
                        item.getMember("strItemName").toString()));
        } catch (PolyglotException e) {
            LOG.debug("Unable to parse item.", e);
            return Optional.empty();
        } catch (IOException e) {
            throw new RuntimeException("Unable to parse item.", e);
        }
    }

    public Item parseRegex(String document) {
        //TODO: implement
        throw new RuntimeException("Not yet implemented");
    }

    private String fixOpentipNotDefined(String source) {
        return "var Opentip = {styles: {bricklink:\"\"}, defaultStyle: \"\"};\n" + source;
    }
}
