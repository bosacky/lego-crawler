package sk.bosacky.lego;

import org.dhatim.fastexcel.Workbook;
import org.dhatim.fastexcel.Worksheet;
import sk.bosacky.lego.model.LegoSetInfo;

import javax.enterprise.context.Dependent;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Dependent
public class ExcelExporterFastExcel implements ExcelExporter {

    private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").withZone(ZoneId.systemDefault());

    @Override
    public void exportToExcel(List<LegoSetInfo> infoList, String outputFileName) {
        try (OutputStream os = new FileOutputStream(outputFileName)) {
            Workbook wb = new Workbook(os, "MyApplication", "1.0");

            createPivotSheet(wb, infoList);
            createDataSheet(wb, infoList);

            wb.finish();
        } catch (IOException e) {
            throw new RuntimeException("Error writing output file", e);
        }
    }

    private Worksheet createPivotSheet(Workbook wb, List<LegoSetInfo> infoList) {
        var ws = wb.newWorksheet("Pivot");
        ws.value(0, 0, "Súčet z Parts");
        ws.value(0, 1, "Počet z ID");
        ws.formula(1, 0, "SUM(Data!D:D)");
        ws.value(1, 1, infoList.size());
        return ws;
    }

    private Worksheet createDataSheet(Workbook wb, List<LegoSetInfo> infoList) {
        var ws = wb.newWorksheet("Data");
        var headers = List.of("ID", "Typ", "THEME", "Parts", "Minifigs", "Weight", "PRICE - NEW - SALED", "PRICE - USED - SALED", "PRICE - NEW - FOR SALE", "PRICE - USED - FOR SALE", "WEB");
        for (int i = 0; i < headers.size(); i++) {
            ws.value(0, i, headers.get(i));
        }

        for (int i = 0; i < infoList.size(); i++) {
            var info = infoList.get(i);
            ws.value(i + 1, 0, info.getItemNo());
            ws.value(i + 1, 1, info.getTitle());
            //TODO: theme
            ws.value(i + 1, 3, info.getPartsCount());
            ws.value(i + 1, 4, info.getMinifigsCount());
            ws.value(i + 1, 5, info.getWeight());
            ws.value(i + 1, 6, info.getSalesInfo().getLast6MonthsSalesNew().getAvgPrice());
            ws.value(i + 1, 7, info.getSalesInfo().getLast6MonthsSalesUsed().getAvgPrice());
            ws.value(i + 1, 8, info.getSalesInfo().getCurrentItemsForSaleNew().getAvgPrice());
            ws.value(i + 1, 9, info.getSalesInfo().getCurrentItemsForSaleUsed().getAvgPrice());
            ws.value(i + 1, 10, info.getUrl());
        }

        return ws;
    }

}
