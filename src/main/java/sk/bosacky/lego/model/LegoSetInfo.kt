package sk.bosacky.lego.model

data class LegoSetInfo(
        val title: String,
        val itemNo: String,
        val weight: Int,
        val partsCount: Int,
        val minifigsCount: Int,
        val salesInfo: SalesInfo,
        val url: String)

data class SalesInfo(
        val last6MonthsSalesNew: SalesInfoDetail,
        val last6MonthsSalesUsed: SalesInfoDetail,
        val currentItemsForSaleNew: SalesInfoDetail,
        val currentItemsForSaleUsed: SalesInfoDetail
)

data class SalesInfoDetail(
        val timesSold: Int,
        val totalQty: Int,
        val minPrice: Double,
        val avgPrice: Double,
        val qtyAvgPrice: Double,
        val maxPrice: Double)
