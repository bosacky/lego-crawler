package sk.bosacky.lego.model

data class Item(
        val idItem: Int,
        val type: String,
        val typeName: String,
        val itemno: String,
        val itemnoBase: String,
        val itemStatus: String,
        val invStatus: String,
        val itemSeq: String,
        val idColorDefault: Int,
        val typeImgDefault: String,
        val catID: String,
        val idColorForPG: Int,
        val strMainSImgUrl: String,
        val strMainLImgUrl: String,
        val strLegacyLargeImgUrl: String,
        val strLegacyLargeThumbImgUrl: String,
        val strAssoc1ImgSUrl: String,
        val strAssoc1ImgLUrl: String,
        val strAssoc2ImgSUrl: String,
        val strAssoc2ImgLUrl: String,
        val strItemName: String
)