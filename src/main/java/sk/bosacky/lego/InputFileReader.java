package sk.bosacky.lego;

import org.graalvm.collections.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.Dependent;
import java.io.*;
import java.util.Set;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Dependent
public class InputFileReader {
    private static final Logger LOG = LoggerFactory.getLogger(InputFileReader.class);

    Set<Pair<String, String>> readSetNumberTypes(String fileName) {
        LOG.info("Reading input file {}", fileName);
        var file = new File(fileName);
        return readSetNumberTypes(file);
    }

    Set<Pair<String, String>> readSetNumberTypes(File file) {
        try (var is = new FileInputStream(file); var isr = new InputStreamReader(is); var br = new BufferedReader(isr)) {
            return br.lines().map(l -> l.split(";")).filter(l -> l.length == 2).map(s -> Pair.create(s[0], s[1])).collect(Collectors.toSet());
        } catch (IOException e) {
            throw new RuntimeException("Error reading input file " + file.getName(), e);
        }
    }
}
