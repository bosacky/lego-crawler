package sk.bosacky.lego;

import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sk.bosacky.lego.model.SalesInfo;
import sk.bosacky.lego.model.SalesInfoDetail;

import javax.enterprise.context.Dependent;
import java.util.regex.Pattern;

@Dependent
public class SalesInfoParser {
    private static final Logger LOG = LoggerFactory.getLogger(SalesInfoParser.class);

    @ConfigProperty(name = "bricklink.priceGuideUrl")
    String priceGuideUrl;

    public SalesInfo parse(int idItem) {
        try {
            var doc = Jsoup.connect(priceGuideUrl + idItem).get();
            LOG.info("Parsing sales info for {}", idItem);
            var salesInfo = parse(doc);
            LOG.info("Done parsing sales info for {}", idItem);
            return salesInfo;
        } catch (Exception e) {
            LOG.error("Error parsing sales for {}", idItem, e);
            var salesInfoDetail = new SalesInfoDetail(-1, -1, -1.0, -1.0, -1.0, -1.0);
            return new SalesInfo(salesInfoDetail, salesInfoDetail, salesInfoDetail, salesInfoDetail);
        }
    }

    SalesInfo parse(Document doc) {
        var pricingSummaryTables = doc.select("table.pcipgSummaryTable");
        if (pricingSummaryTables.size() != 4)
            throw new IllegalArgumentException("Expecting 4 pricing summary tables. Have " + pricingSummaryTables.size());

        return new SalesInfo(
                parseSalesInfoDetail(pricingSummaryTables.get(0), false),
                parseSalesInfoDetail(pricingSummaryTables.get(1), false),
                parseSalesInfoDetail(pricingSummaryTables.get(2), true),
                parseSalesInfoDetail(pricingSummaryTables.get(3), true)
        );
    }

    private SalesInfoDetail parseSalesInfoDetail(Element pricingSummaryTable, boolean isCurrent) {
        var timesSold = parseIntegerProperty(isCurrent ? "Total Lots" : "Times Sold", pricingSummaryTable);
        var totalQty = parseIntegerProperty("Total Qty", pricingSummaryTable);
        var minPrice = parseDoubleProperty("Min Price", pricingSummaryTable);
        var avgPrice = parseDoubleProperty("Avg Price", pricingSummaryTable);
        var qtyAvgPrice = parseDoubleProperty("Qty Avg Price", pricingSummaryTable);
        var maxPrice = parseDoubleProperty("Max Price", pricingSummaryTable);

        return new SalesInfoDetail(timesSold, totalQty, minPrice, avgPrice, qtyAvgPrice, maxPrice);
    }

    private int parseIntegerProperty(String propertyName, Element pricingSummaryTable) {
        return Integer.parseInt(parseStringProperty(propertyName, pricingSummaryTable));
    }

    private double parseDoubleProperty(String propertyName, Element pricingSummaryTable) {
        return parseNumber(parseStringProperty(propertyName, pricingSummaryTable));
    }

    private String parseStringProperty(String propertyName, Element pricingSummaryTable) {
        return pricingSummaryTable.select("td:contains(" + propertyName + ")").stream().findFirst().map(Element::nextElementSibling)
                .map(Element::text).orElseThrow(() -> new IllegalArgumentException("Unable to locate " + propertyName));
    }

    private double parseNumber(String text) {
        var p = Pattern.compile("([A-Za-z]*)\\s*(\\d+\\.{0,1}\\d*)");
        var m = p.matcher(text);
        if (!m.find()) {
            LOG.warn("Unable to parse {}", text);
            return -1.0;
        } else {
            if (m.group(1) != null && !m.group(1).isEmpty() && !m.group(1).equals("EUR"))
                LOG.warn("The currency is not EUR! It's {}", m.group(1));
            return Double.parseDouble(m.group(2));
        }
    }
}
