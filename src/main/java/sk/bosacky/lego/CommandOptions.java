package sk.bosacky.lego;

import org.graalvm.collections.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;
import java.util.Optional;
import java.util.function.BinaryOperator;
import java.util.stream.Collectors;

public class CommandOptions {

    private static final Logger LOG = LoggerFactory.getLogger(CommandOptions.class);

    private final Map<CommandOption, String> commands;

    public CommandOptions(String... args) {
        commands = parse(args);
    }

    private Map<CommandOption, String> parse(String[] args) {
        var reduced = Arrays.stream(args).reduce(new ArrayList<Pair<CommandOption, String>>(),
                (pairs, s) -> {
                    var lastIndex = pairs.size() - 1;
                    if (s.startsWith("-") || s.startsWith("--")) {
                        CommandOption.find(s).map(c -> Pair.create(c, "")).ifPresent(pairs::add);
                    } else if (lastIndex >= 0) {
                        var lastPair = pairs.get(lastIndex);
                        if (!lastPair.getRight().isEmpty()) {
                            LOG.warn("{} already assigned: {}. New value {} will be ignored.",
                                    lastPair.getLeft(), lastPair.getRight(), s);
                        } else {
                            pairs.set(lastIndex, Pair.create(lastPair.getLeft(), s));
                        }
                    }
                    return pairs;
                },
                unsupportedThrowingCombiner());

        return reduced.stream().collect(Collectors.toMap(Pair::getLeft, Pair::getRight));
    }

    public int size() {
        return commands.size();
    }

    public boolean hasOption(CommandOption option) {
        return commands.containsKey(option);
    }

    public Optional<String> getOption(CommandOption option) {
        return Optional.ofNullable(commands.get(option));
    }

    private <T> BinaryOperator<T> unsupportedThrowingCombiner() {
        return (m1, m2) -> {
            throw new UnsupportedOperationException("Parallel execution not supported.");
        };
    }
}
