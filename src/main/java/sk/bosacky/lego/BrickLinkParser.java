package sk.bosacky.lego;

import io.smallrye.mutiny.Uni;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.graalvm.collections.Pair;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Evaluator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sk.bosacky.lego.model.LegoSetInfo;

import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Dependent
public class BrickLinkParser {
    private static final Logger LOG = LoggerFactory.getLogger(BrickLinkParser.class);

    @ConfigProperty(name = "bricklink.mainUrl")
    String bricklinkUrl;

    @Inject
    ItemParser itemParser;

    @Inject
    SalesInfoParser salesInfoParser;

    public Uni<LegoSetInfo> parse(String itemNo, String itemTypeCode) {
        return Uni.createFrom().completionStage(() -> CompletableFuture.supplyAsync(() -> {
            try {
                LOG.info("Parsing main info for {}", itemNo);
                var url = bricklinkUrl + "?" + itemTypeCode + "=" + itemNo;
                LOG.info("Got URL {}", url);
                var doc = Jsoup.connect(url).get();
                var legoSetInfo = parse(itemNo, doc);
                LOG.info("Done parsing main info for {}", itemNo);
                return legoSetInfo;
            } catch (Exception e) {
                LOG.error("Error parsing for itemNo: {}", itemNo, e);
                return null;
            }
        }));
    }

    LegoSetInfo parse(String itemNo, Document doc) {
        var title = doc.select(new Evaluator.Id("item-name-title")).stream().findFirst().map(Element::text).orElseThrow(() -> new RuntimeException("Lego title not found"));
        var weight = doc.select(new Evaluator.Id("item-weight-info")).stream().findFirst().map(Element::text).map(this::parseCountTuple).map(Pair::getRight).orElse(0);

        var consistOfEl = doc.select("strong:contains(Item Consists Of)");
        var countsMap = consistOfEl.first().siblingElements().stream().filter(n -> n.is("a"))
                .map(Element::text).map(this::parseCountTuple)
                .collect(Collectors.toMap(Pair::getLeft, Pair::getRight));

        int partsCount = countsMap.getOrDefault("Parts", 0);
        int minifigsCount = countsMap.getOrDefault("Minifigs", countsMap.getOrDefault("Minifig", 0));

        var item = doc.select("script").stream()
                .filter(el -> el.childNodeSize() > 0)
                .map(el -> el.childNodes().get(0).toString())
                .map(scr -> itemParser.parseJs(scr))
                .filter(Optional::isPresent)
                .map(Optional::get)
                .findFirst().orElseGet(() -> itemParser.parseRegex(doc.toString()));

        //TODO: make async as well
        var salesInfo = salesInfoParser.parse(item.getIdItem());
        return new LegoSetInfo(title, itemNo, weight, partsCount, minifigsCount, salesInfo, doc.baseUri());
    }

    Pair<String, Integer> parseCountTuple(String text) {
        var p = Pattern.compile("(\\d+)\\s*([a-zA-Z]+)");
        var m = p.matcher(text);
        if (!m.find()) {
            LOG.warn("Unable to parse " + text);
            return Pair.empty();
        } else {
            return Pair.create(m.group(2), Integer.parseInt(m.group(1)));
        }
    }
}
