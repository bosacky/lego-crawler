package sk.bosacky.lego;

import sk.bosacky.lego.model.LegoSetInfo;

import java.util.List;

public interface ExcelExporter {
    void exportToExcel(List<LegoSetInfo> infoList, String outputFileName);
}
