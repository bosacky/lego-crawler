package sk.bosacky.lego;

import java.util.Arrays;
import java.util.Optional;

public enum CommandOption {
    INPUT("input", "i"),
    OUTPUT("output", "o"),
    HELP("help", "h");

    public final String fullName;
    public final String shortName;

    CommandOption(String fullName, String shortName) {
        this.fullName = fullName;
        this.shortName = shortName;
    }

    static Optional<CommandOption> find(String shortOrFullName) {
        var sofn = shortOrFullName.replace("-", "").trim();
        return Arrays.stream(CommandOption.values())
                .filter(co -> co.shortName.equalsIgnoreCase(sofn) || co.fullName.equalsIgnoreCase(sofn))
                .findFirst();
    }
}
