package sk.bosacky.lego;

import io.quarkus.runtime.QuarkusApplication;
import io.quarkus.runtime.annotations.QuarkusMain;
import io.smallrye.mutiny.Multi;
import org.graalvm.collections.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.inject.Named;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Set;

@QuarkusMain
public class LegoCrawlerMain implements QuarkusApplication {

    private static final Logger LOG = LoggerFactory.getLogger(LegoCrawlerMain.class);

    private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd_HHmmss").withZone(ZoneId.systemDefault());

    @Inject
    InputFileReader reader;

    @Inject
    BrickLinkParser brickLinkParser;

    @Inject
    ExcelExporter exporter;

    @Override
    public int run(String... args) {
        var commandOptions = new CommandOptions(args);

        if (commandOptions.hasOption(CommandOption.HELP)) {
            System.out.println("Usage: lego-crawler [options...]\n" +
                    "-i, --input <filename> Specifies input csv file\n" +
                    "-o, --output <filename> Specifies output excel file\n" +
                    "-h, --help Shows this help\n\n" +
                    "...press any key to continue...");
            try {
                System.in.read();
            } catch (IOException e) {
                LOG.error("Error reading key", e);
            }
        } else {
            var setNumberTypePairs = commandOptions.getOption(CommandOption.INPUT)
                    .map(reader::readSetNumberTypes)
                    .orElseThrow(() -> new IllegalArgumentException("Input file not specified."));

            var list = Multi.createFrom().items(setNumberTypePairs.stream())
                    .onItem().transformToUniAndMerge(p -> brickLinkParser.parse(p.getLeft(), p.getRight()))
                    .collectItems()
                    .asList().await().indefinitely();

            var fileName = commandOptions.getOption(CommandOption.OUTPUT).orElseGet(() -> "lego-" + DATE_TIME_FORMATTER.format(Instant.now()) + ".xlsx");
            exporter.exportToExcel(list, fileName);
        }
        return 0;
    }


}
